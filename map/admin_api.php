<?php
session_start();

include_once 'config.php';

header("Content-Type: text/plain ; charset=utf-8");
header("Cache-Control: no-cache , private");//anti Cache pour HTTP/1.1
header("Pragma: no-cache");//anti Cache pour HTTP/1.0

// $response sera le tableau que l'ont transformera en json à la fin du traitement
$response = array();
$response["success"] = false;

// If the user isn't allowed to query this api, stop and return an error message
if (isset($_SESSION['user']) && $_SESSION['admin'] == true) {

    // Do nothing, the user can access the api as an admin

} elseif (isset($_SESSION['user']) && $_SESSION['geoadmin'] == true) {
    
    // Do nothing, the user can access the api as a geoadmin

} else { // The user does not have the necessary rights to access the administration panel
    $response["error"] = "User not authenticated";
    pg_close($dbconn); 
    echo json_encode($response) ;
    exit; 
}

// Connect to database
$conn_string = "host=".$_SESSION['db_host']." port=".$_SESSION['db_port']." dbname=".$_SESSION['db_name']." user=".$_SESSION['db_user']." password=".$_SESSION['db_password'];
$dbconn = pg_connect($conn_string);
if (!$dbconn) {
    $response["error"] = "Database connection failed";
    pg_close($dbconn); 
    echo json_encode($response) ;
    exit;
}

// Define a "noreply" adress to send the mails from
$email_noreply = "noreply@teledetection.fr";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// If the user is an administrator, allow him to load a list of all users from the database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_SESSION['admin']) and ($_SESSION['admin']== true) and 
    isset($_POST["mode"]) and ($_POST["mode"] == 'fetch_userdata')) {
    
    // The user is authenticated as an administrator, we can load the list of users from the database
    // We send the user's id as an answer as it is needed for further processing
    $response["admin_id"] = $_SESSION["user_id"];

    // We start with loading all the pending users
    $sql = "SELECT id, username, role, motivation FROM userdata.users WHERE user_enabled = false ORDER BY id";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result)<>-1) {

        // If the query worked, we store all the elements in an array
        if (isset($pending_users)) {
            unset($pending_users);
        }

        $response["success"] = true;
        $pending_users = array();

        while ($array = pg_fetch_array($result)) {

            $user = (object) array(
                'id' => $array["id"],
                'username' => htmlspecialchars_decode($array["username"]),
                'role' => $array["role"],
                'motivation' => htmlspecialchars_decode($array["motivation"])
            );

            array_push($pending_users, $user);
        }
        
        $response["pending_users"] = $pending_users;


    } else {
        // If the query didn't return anything, send an error message
        $response["success"] = false;
        $response["error"] = "There was an error while fetching pending users.";
    }

    // Then we load all the allowed users
    $sql = "SELECT id, username, role, motivation FROM userdata.users WHERE user_enabled = true ORDER BY id";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result)<>-1) {

        // If the query worked, we store all the elements in an array
        if (isset($allowed_users)) {
            unset($allowed_users);
        }

        $response["success"] = true;
        $allowed_users = array();

        while ($array = pg_fetch_array($result)) {

            $user = (object) array(
                'id' => $array["id"],
                'username' => htmlspecialchars_decode($array["username"]),
                'role' => $array["role"],
                'motivation' => htmlspecialchars_decode($array["motivation"])
            );

            array_push($allowed_users, $user);
        }
        
        $response["allowed_users"] = $allowed_users;

    } else {
        // If the query didn't return anything, send an error message
        $response["success"] = false;
        $response["error"] = "There was an error while fetching allow users.";
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Allow a new user to use the database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

elseif (isset($_SESSION['admin']) and ($_SESSION['admin']== true) and isset($_POST["id"]) and
        isset($_POST["mode"]) and ($_POST["mode"] == 'allow_user')) {
   
    // Update the user's authorizations in the database
    $sql = "UPDATE userdata.users SET user_enabled=true WHERE id=".$_POST["id"]." RETURNING *";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result) == 1) { // The field was updated with no error

        $response["success"] = true;

        // We send a mail to the user to notify him/her
        $row = pg_fetch_array($result);
        $email = $row["email"];

        $response["email"] = $email;

        $to      = $email;
        $subject = 'Autorisation NAMO Geoweb - RestEAUr\'Lag';
        $message = 'Un administrateur a activé votre compte. Vous pouvez désormais utiliser la plateforme NAMO Geoweb - RestEAUr\'Lag accessible à cette adresse : https://rivage-guadeloupe.teledetection.fr/';
        $headers = "From: ".$email_noreply."\r\n" .
        "Reply-To: ".$email_noreply."\r\n" .
        'X-Mailer: PHP/' . phpversion();

        if (!mail($to, $subject, $message, $headers)) {
            $response["success"] = false;
            $response["error_code"] = 2 ;
            $response["error"] = "There was a problem with sending the mail to the user. Please contact him directly at the following email address: "+$email;
        }

    } else {
        $response["error"] = "There was an error allowing the user to use the application";
        $response["error_code"] = 1;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Reject an user and delete him from the database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

elseif (isset($_SESSION['admin']) and ($_SESSION['admin']== true) and isset($_POST["id"]) and
        isset($_POST["mode"]) and ($_POST["mode"] == 'reject_user')) {
   
    // Delete the user from the database
    $sql = "DELETE FROM userdata.users WHERE id=".$_POST["id"]." RETURNING *";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result) == 1) { // The field was updated with no error

        $response["success"] = true;

        // We send a mail to the user to notify him/her
        $row = pg_fetch_array($result);
        $email = $row["email"];

        $response["email"] = $email;

        $to      = $email;
        $subject = 'Autorisation NAMO Geoweb - RestEAUr\'Lag';
        $message = 'Votre demande de création d\'un compte pour accéder aux outils participatifs de la plateforme NAMO Geoweb - RestEAUr\'Lag a été refusée par un administrateur de la plateforme.';
        $headers = "From: ".$email_noreply."\r\n" .
        "Reply-To: ".$email_noreply."\r\n" .
        'X-Mailer: PHP/' . phpversion();

        if (!mail($to, $subject, $message, $headers)) {
            $response["success"] = false;
            $response["error_code"] = 2 ;
            $response["error"] = "There was a problem with sending the mail to the user. Please contact him directly at the following email address: "+$email;
        }

    } else {
        $response["error"] = "There was an error allowing the user to use the application";
        $response["error_code"] = 1;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Revoke the user's rights to access the application
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

elseif (isset($_SESSION['admin']) and ($_SESSION['admin']== true) and isset($_POST["id"]) and
        isset($_POST["mode"]) and ($_POST["mode"] == 'revoke_user')) {
   
    // Update the user's authorizations in the database
    $sql = "UPDATE userdata.users SET user_enabled = false WHERE id=".$_POST["id"]." RETURNING *";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result) == 1) { // The field was updated with no error

        $response["success"] = true;

        // We send a mail to the user to notify him/her
        $row = pg_fetch_array($result);
        $email = $row["email"];

        $response["email"] = $email;

        $to      = $email;
        $subject = 'Autorisation NAMO Geoweb - RestEAUr\'Lag';
        $message = 'Votre compte sur la plateforme NAMO Geoweb - RestEAUr\'Lag a été temporairement suspendu par un administrateur.';
        $headers = "From: ".$email_noreply."\r\n" .
        "Reply-To: ".$email_noreply."\r\n" .
        'X-Mailer: PHP/' . phpversion();

        if (!mail($to, $subject, $message, $headers)) {
            $response["success"] = false;
            $response["error_code"] = 2 ;
            $response["error"] = "There was a problem with sending the mail to the user. Please contact him directly at the following email address: "+$email;
        }

    } else {
        $response["error"] = "There was an error revoking the user's rights.";
        $response["error_code"] = 1;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Edit an user's role
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

elseif (isset($_SESSION['admin']) and ($_SESSION['admin']== true) and isset($_POST["id"]) and
        isset($_POST["role"]) and isset($_POST["mode"]) and ($_POST["mode"] == 'edit_user_role')) {
   
    // Update the user's role in the database
    $sql = "UPDATE userdata.users SET role = '".$_POST["role"]."' WHERE id=".$_POST["id"]." RETURNING *";
    $result = pg_query($dbconn,$sql);

    if (pg_num_rows($result) == 1) { // The field was updated with no error

        $response["success"] = true;

        // We send a mail to the user to notify him/her
        $row = pg_fetch_array($result);
        $email = $row["email"];

        $response["email"] = $email;

        $to      = $email;
        $subject = 'Rôle NAMO Geoweb - RestEAUr\'Lag';
        $message = 'Un administrateur a édité votre rôle sur la plateforme NAMO Geoweb - RestEAUr\'Lag. Vous êtes désormais ' + $_POST["role"] + '.';
        $headers = "From: ".$email_noreply."\r\n" .
        "Reply-To: ".$email_noreply."\r\n" .
        'X-Mailer: PHP/' . phpversion();

        if (!mail($to, $subject, $message, $headers)) {
            $response["success"] = false;
            $response["error_code"] = 2 ;
            $response["error"] = "There was a problem with sending the mail to the user. Please contact him directly at the following email address: "+$email;
        }

    } else {
        $response["error"] = "There was an error updating the user's rights. Please reload the application and try again.";
        $response["error_code"] = 1;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Api called with no or unknown mode
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
else {
    $response["error"] = "No mode defined or mode unknown"; 
    $response["sentMode"] = $_POST["mode"];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script end : close database connexion and encode json response
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// fermeture de la liaison avec la base de données
pg_close($dbconn); 

// quel que soit le traitement réalisé (ou non), on retourne $response que l'on encode en json auparavant
echo json_encode($response) ;

?>