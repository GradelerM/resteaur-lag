/*
===============================================================================
Initializing the map and popups
===============================================================================
*/
// Defining main map max bounds (so we can't pan outside)
var maxBoundsX = ol.proj.fromLonLat([1.0, 41.0]);
var maxBoundsY = ol.proj.fromLonLat([13.0, 44.80]);
var maxBounds = maxBoundsX.concat(maxBoundsY);

var mainView = new ol.View({
    center: ol.proj.fromLonLat([6.8, 43.04]),
    zoom: 4,
    minZoom: 7,
    maxZoom: 16,
    extent: maxBounds
});

// Creating the map
var map = new ol.Map({
  target: 'map',
  view: mainView,
  controls: ol.control.defaults().extend([
    new ol.control.ScaleLine()
  ])
});

/*
===============================================================================
Emprises
===============================================================================
*/
// Emprises and zooms
// [ol.proj.fromLonLat([lon, lat]), zoom]
// lon and lat in EPSG:4326 (WGS84, NOT web-mercator)
// zoom is the desired zoom level
var empriseMediterrannee       = [ol.proj.fromLonLat([6.8, 43.04]), 5];
var empriseLaguneBiguglia      = [ol.proj.fromLonLat([9.46, 42.59]), 12];
var empriseOccitanieEst        = [ol.proj.fromLonLat([3.9, 43.6]), 9.5];
var empriseEtangOr             = [ol.proj.fromLonLat([4.05, 43.6]), 11.5];
var empriseEtangsPalavasiens   = [ol.proj.fromLonLat([3.86, 43.51]), 11.5];
// Insert your new emprise bounds here

/*
===============================================================================
Fonds de Carte
===============================================================================
*/
// IGN resolutions for displaying different levels of detail
var ignResolutions = [
    156543.03392804103,
    78271.5169640205,
    39135.75848201024,
    19567.879241005125,
    9783.939620502562,
    4891.969810251281,
    2445.9849051256406,
    1222.9924525628203,
    611.4962262814101,
    305.74811314070485,
    152.87405657035254,
    76.43702828517625,
    38.218514142588134,
    19.109257071294063,
    9.554628535647034,
    4.777314267823517,
    2.3886571339117584,
    1.1943285669558792,
    0.5971642834779396,
    0.29858214173896974,
    0.14929107086948493,
    0.07464553543474241
] ;

// Set Open Street Map layer
var planOSM = new ol.layer.Tile({
  source: new ol.source.OSM({}),
  visible: true,
  title: 'planOSM'
});

// Set ESRI imagery layer
var planESRI = new ol.layer.Tile({
  source: new ol.source.XYZ({
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    attributions: 'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
          'rest/services/World_Imagery">ArcGIS</a>'
  }),
  visible: true,
  title: 'planESRI'
});

// Set IGN imagery layer
var planIGNimagerie = new ol.layer.Tile({
  source: new ol.source.WMTS({
    url: 'https://wxs.ign.fr/d0yd9eg2ld67ike7fy756ano/geoportail/wmts',
    layer: 'ORTHOIMAGERY.ORTHOPHOTOS',
    matrixSet: 'PM',
    format: 'image/jpeg',
    style: 'normal',
    attributions: 'IGN-F/Géoportail',
    tileGrid: new ol.tilegrid.WMTS({
      origin: [-20037508,20037508], // topLeftCorner
                              resolutions: ignResolutions, // changing resolutions
                              matrixIds: ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"] // ids des TileMatrix
    })
  })
});

// Set IGN topo layer
var planIGN = new ol.layer.Tile({
  source: new ol.source.WMTS({
    url: 'https://wxs.ign.fr/d0yd9eg2ld67ike7fy756ano/geoportail/wmts',
    layer: 'GEOGRAPHICALGRIDSYSTEMS.MAPS',
    matrixSet: 'PM',
    format: 'image/jpeg',
    style: 'normal',
    attributions: 'IGN-F/Géoportail',
    tileGrid: new ol.tilegrid.WMTS({
      origin: [-20037508,20037508], // topLeftCorner
                              resolutions: ignResolutions, // changing resolutions
                              matrixIds: ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"] // ids des TileMatrix
    })
  })
});

// OSM layer displayed by default
map.addLayer(planOSM);

// All basmap layers in one array (needed for looping though these)
var basemaps = [
  'planOSM',
  'planESRI',
  'planIGNimagerie',
  'planIGN'
];


/*
===============================================================================
Communes "invisible" layer: hover and select effects
===============================================================================
*/
/*https://resteaur-lag.teledetection.fr/geoserver/wfs?service=WFS&version=1.1.0&request=GetFeature&typename=tetis:bd_topo_communes&outputFormat=application/json&PROPERTYNAME=nom&CQL_FILTER=nom=%27Mauguio%27*/

/*
// Communes WFS base layer
var communesWFS = new ol.source.Vector({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wfs?service=WFS' +
  '&version=1.1.0&request=GetFeature&typename=tetis:bd_topo_communes' +
  '&outputFormat=application/json' ,
  format: new ol.format.GeoJSON(),
  serverType: 'geoserver'
});
var communes = new ol.layer.Vector({
  source: communesWFS,
  visible: true,
  opacity: 0.5,
  zIndex: 1001,
  id: 'communes', // MUST be the same as the var name
  theme: 'UnitesAdministratives',
  title: 'Communes (BD TOPO)',
  widgets: ''
});
map.addLayer(communes);
*/

// Creating empty layer for communes selection
/*
var communeSelectedSource = new ol.source.Vector();
var communeSelected = new ol.layer.Vector({
  source: communeSelectedSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 255, 1.0)',
      width: 2,
    }),
  }),
});
*/

/*
===============================================================================
Couches : layers
===============================================================================
*/

// end of testing layers

// bd topo communes : WMS layer
var bd_topo_communesWMS = new ol.source.ImageWMS({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wms',
  params: {'LAYERS': 'tetis:bd_topo_communes', 'TILED':true},
  serverType: 'geoserver'
});
var bd_topo_communes = new ol.layer.Image({
  source: bd_topo_communesWMS,
  visible: false,
  opacity: 0.5,
  zIndex: 1000,
  id: 'bd_topo_communes', // MUST be the same as the var name
  theme: 'UnitesAdministratives',
  title: 'BD Topo : communes',
  widgets: 'opacity legend',
  debutTemp: '1900',
  finTemp: '2020'
});

// bdoh_herault : WMS layer
var bdoh_heraultWMS = new ol.source.ImageWMS({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wms',
  params: {'LAYERS': 'tetis:bdoh_herault', 'TILED':true},
  serverType: 'geoserver'
});
var bdoh_herault = new ol.layer.Image({
  source: bdoh_heraultWMS,
  visible: false,
  opacity: 1,
  zIndex: 1001,
  id: 'bdoh_herault', // MUST be the same as the var name
  theme: 'OrthoImagerie',
  title: 'bdoh Hérault',
  widgets: 'opacity',
  debutTemp: '1900',
  finTemp: '2020'
});

// pvah : WMS layer
var pvahWMS = new ol.source.ImageWMS({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wms',
  params: {'LAYERS': 'tetis:ifremer_pvah_herault', 'TILED':true},
  serverType: 'geoserver'
});
var pvah = new ol.layer.Image({
  source: pvahWMS,
  visible: false,
  opacity: 1,
  zIndex: 1001,
  id: 'pvah', // MUST be the same as the var name
  theme: 'OrthoImagerie',
  title: 'pvah Hérault',
  widgets: 'opacity',
  debutTemp: '800',
  finTemp: '900'
});

// pop_ancienne_occitanie : WFS layer
var pop_ancienne_occitanieWFS = new ol.source.Vector({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wfs?service=WFS&' +
  'version=1.1.0&request=GetFeature&typename=tetis:pop_ancienne_occitanie&' +
  'outputFormat=application/json',
  format: new ol.format.GeoJSON(),
  serverType: 'geoserver'
});
var pop_ancienne_occitanie = new ol.layer.Vector({
  source: pop_ancienne_occitanieWFS,
  visible: false,
  opacity: 1,
  zIndex: 1002,
  id: 'pop_ancienne_occitanie', // MUST be the same as the var name
  theme: 'Demographie',
  title: 'Population par année de rencensement',
  widgets: 'opacity customlegend',
  // As 'customlegend' widget is active, we MUST define a 'customlegendTitle'
  customlegendTitle: "Nombre d'habitants par commune en ", // Title appearing on the custom legend
  debutTemp: '1900',
  finTemp: '2020'
});

// Occupation du sol en 1963, niveau 1
var os_etang_orWMS = new ol.source.ImageWMS({
  url: 'https://resteaur-lag.teledetection.fr/geoserver/wms',
  params: {'LAYERS' : 'tetis:os_etang_or_date:1963_niv:1',
           'TILED' : true
          },
  serverType: 'geoserver'
});
var os_etang_or = new ol.layer.Image({
  source: os_etang_orWMS,
  visible: false,
  opacity: 1,
  zIndex: 1001,
  id: 'os_etang_or', // MUST be the same as the var name
  theme: 'OccupationDuSol',
  title: "Occupation du sol autour de l'étang de l'Or",
  widgets: 'opacity legend choixAnnee legendLevel',
  choixAnnee: '1963 1977 2017',
  debutTemp: '1963',
  finTemp: '1977'
});


/*
===============================================================================
Couches : layer groups
===============================================================================
*/
// It is advised to always name layers groups starting with the string "theme"
// For example : themeMonTheme or themeMyTheme
// If not, the application will send a warning and ask you to nename these themes
// For further information, check the documentation

// INSPIRE theme : "unités administratives"
var themeUnitesAdministratives = new ol.layer.Group({
  name: "Unités administratives",
  layers: [
    bd_topo_communes,
  ],
});

// INSPIRE theme : "ortho-imagerie"
var themeOrthoImagerie = new ol.layer.Group({
  name: "Ortho-imagerie",
  layers: [
    bdoh_herault,
    pvah
  ],
});

// INSPIRE theme : "démographie"
var themeDemographie = new ol.layer.Group({
  name: "Démographie",
  layers: [
    pop_ancienne_occitanie
  ],
});

// INSPIRE theme : "occupation du sol"
var OccupationDuSol = new ol.layer.Group({

  layers: [
    os_etang_or
  ],
});

// Add new layer group here

// All layer groups names in one array
var layerGroups = [
  'themeUnitesAdministratives',
  'themeOrthoImagerie',
  'themeDemographie',
  'OccupationDuSol'

  // Add your layer group's var name here
];

// Adding layers to the map
function addingLayers(e) {
  for ( var i = 0; i < e.length; i++ ) {
    var layer = window[e[i]];
    map.addLayer(layer);
  }
};
addingLayers(layerGroups);

