jQuery(document).ready(function(){

    /* 
    =================================================================
    Defining variables to be used later in the functions
    =================================================================
    */
    // Defining the available roles
    var roles = [
        ['admin', 'Administrateur'],
        ['editor', 'Editeur'],
        ['contributor', 'Contributeur']
    ];

    /* 
    =================================================================
    Swap between different tabs 
    =================================================================
    */
    $('.tab').click(function() {
        var tabId = $(this).attr('id');
        var contentId = tabId.replace("tab", "content");
        $('.tab').each(function() {
            $(this).addClass("disabled");
        });
        $('.content-window').each(function() {
            $(this).addClass("hidden");
        });
        $('#' + tabId).removeClass("disabled");
        $('#' + contentId).removeClass("hidden");
    });

    /* 
    =================================================================
    Define buttons to display in the interface tables
    =================================================================
    */
    // 'type' can be 'positive', 'negative' or 'neutral' (or match any other style defined later in the css)
    // it matches the backoffice.css styles button.positive and button.negative
    // 'content' must be the string that will be displayed on the button
    
    // And additional class can be added in 'type', see example below

    // Use example : addButton('positive authorize', 'Autoriser');
    // See in the function fetchUserData() for more examples

    function addButton(type, content) {
        var btn = '<button class="' + type + '">'+ content +'</button>';
        return btn;
    }

    /* 
    =================================================================
    Define main reusable elements
    =================================================================
    */
    // Main modal for displaying information messages
    function displayMessageModal(message) {
        $("#modal-admin-interface").dialog({
            modal: true,
            close: function () {
                $(this).empty();
            }
        }).append(message);
    };

    // Error message modal
    function displayErrorModal(message) {
        $("#modal-admin-interface-error").dialog({
            modal: true,
            close: function () {
                $(this).empty();
            }
        }).append(message);
    }

    /* 
    =================================================================
    Load users informations (username, motives, possibility to allow or deny access to the application)
    =================================================================
    */
    // Function to empty the tables (needed for reloading)
    function emptyUserTables() {
        $('#pending-users-list thead').empty();
        $('#pending-users-list tbody').empty();
        $('#allowed-users-list thead').empty();
        $('#allowed-users-list tbody').empty();
    }
    
    // We check if the user is an administrator in the api (admin_api.php)
    function fetchUserData() {
        
        // Send the query to the api
        $.ajax({
            url: './admin_api.php',
            method: 'POST',
            async: true,
            data: {
                mode: 'fetch_userdata'
            },
            dataType: 'json',
            beforeSend: function() {
                // Show the loaders
                $('#pending-users-list .loader-container').show();
                $('#allowed-users-list .loader-container').show();

                // Empty the tables (comes in handy when reload is needed)
                emptyUserTables();

            },
            success: function(response) {

                // Make sure the query returned no errors
                if (response.success == true) {
                    
                    // Add the pending user's table head unless it is empty
                    if (response.pending_users.length == 0) { // No pending users, display a message

                        $('#pending-users-list thead').append("<i>Aucun utilisateur en attente</i>");
                        
                    } else { // Pending users, display the table's header
                        var table_head =
                            '<tr>' +
                                '<td>Utilisateur</td>' +
                                '<td>Motif</td>' +
                            '</tr>';
                        $('#pending-users-list thead').append(table_head);
                    }
                    
                    // Display all of the pending users in the corresponding table
                    for (let i = 0; i < response.pending_users.length; i++) {
                        var user = response.pending_users[i];

                        var table_row = 
                            '<tr id="userid-' + user.id + '">' + // We give this row and id = user id
                                '<td class="username">' + user.username + '</td>' +
                                '<td class="user-motivation">' + user.motivation + '</td>' +
                                '<td>' + addButton('positive allow_user', 'Autoriser') + '</td>' +
                                '<td>' + addButton('negative reject_user', 'Refuser') + '</td>' +
                            '</tr>';
                        
                        // Append the row to the existing table
                        $('#pending-users-list tbody').append(table_row);

                    }

                    // Add the allowed user's table head unless it is empty (this shouldn't happen, but just in case...)
                    if (response.allowed_users.length == 0) { // No pending users, display a message

                        $('#pending-users-list thead').append("<i>Aucun utilisateur</i>");
                        
                    } else { // Allowed users, display the table's header
                        var table_head =
                            '<tr>' +
                                '<td>Utilisateur</td>' +
                                '<td>Rôle</td>' +
                                '<td>Motif</td>' +
                            '</tr>';
                        $('#allowed-users-list thead').append(table_head);
                    }

                    // Function to write the "select" element for roles
                    // user_id = the current administrator's id
                    // id = the user's id
                    // role = the user's current role
                    function selectUserRoles(admin_id, id, role) {

                        // We create the list only if user_id =/= online user 
                        // So an admin cannot edit his own role
                        // And we always have at least one admin available on the platform
                        if (id == admin_id) {

                            // Look for the right role to display above
                            for (let i = 0; i < roles.length; i++) {
                                if (roles[i][0] == role) {
                                    var result = roles[i][1];
                                }
                            }

                        } else { // Now create the select lists for all the other users
                            var result = '<div class="flex"><select name="select-user-role" class="select-user-role" disabled>';

                            // Create a select option for each role available
                            for (let i = 0; i < roles.length; i++) {

                                // If this is already the user's role, add the "select" tag to it
                                if (roles[i][0] == role) {
                                    result = result + '<option value="' + roles[i][0] + '" selected>' + roles[i][1] + '</option>';
                                } else {
                                    result = result + '<option value="' + roles[i][0] + '">' + roles[i][1] + '</option>';
                                }
                            }

                            // And close the select tag and the "edit" button
                            result = result + '</select>';
                            result = result + '<a href="#" class="edit-user-role-toggle"><svg><use xlink:href="#iconeParameters" /></use></svg></a>';
                            result = result + '</div>';
                        }

                        return result;
                    };

                    // Display all the allowed users in the corresponding table
                    for (let i = 0; i < response.allowed_users.length; i++) {
                        var user = response.allowed_users[i];
                        var table_row = 
                            '<tr id="userid-' + user.id + '">' + // We give this row and id = user id
                                '<td class="username">' + user.username + '</td>' +
                                '<td class="user-role">' + 
                                    selectUserRoles(response.admin_id, user.id, user.role) +
                                '</td>' +
                                '<td class="user-motivation">' + user.motivation + '</td>';

                        // Only display the "revoke_user" button for other users than the current one
                        // So he can't "auto-ban" himself
                        if (response.admin_id == user.id) {
                            table_row = table_row + '<td></td>';
                        } else {
                            table_row = table_row + '<td>' + addButton('negative revoke_user', 'Retirer') + '</td>';
                        }

                            table_row = table_row + '</tr>';
                        
                        // Append the row to the existing table
                        $('#allowed-users-list tbody').append(table_row);
                    }

                } else { // There was an error

                    // Display the error message
                    var message = 
                        "<p>Une erreur s'est produite :</p>" +
                        "<p>" + response.error + "</p>"; 
                    displayMessageModal(message);

                }

            },
            // Remove the loader once the query is completed
            complete: function() {
                // Hide the loaders
                $('#pending-users-list .loader-container').hide();
                $('#allowed-users-list .loader-container').hide();
            },
            error: function(response) {
                console.error(response.error);
            }
        });

    };
    fetchUserData();

    /* 
    =================================================================
    Interactions to enable a new user
    =================================================================
    */
    // Function to allow an user to auth to the app
    function allowUser(user_id) {
        
        // Send the request to the server
        $.ajax({
            url: './admin_api.php',
            method: 'POST',
            async: true,
            data: {
                mode: 'allow_user',
                id: user_id
            },
            dataType: 'json',
            beforeSend: function() {
                // Do sth
            },
            success: function(response) {
               
                // Check if any error happened
                if (response.success == true || response.success == false && response.error_code == 2) { // No error happened, we can keep processing

                    // Display the modal with a successful message
                    var message = "<p>L'utilisateur peut maintenant s'authentifier sur l'application</p>"; 
                    displayMessageModal(message);

                    // Reload the tables
                    fetchUserData();
                    
                } else { // An error happened, display 
                    
                    console.error(response.error);
                    
                    var message = 
                        "<p>Une erreur s'est produite :</p>" +
                        "<p>" + response.error + "</p>"; 
                    displayMessageModal(message);

                }

            },
            complete: function() {
                // Do sth
            },
            error: function(response) {
                console.error(response.error);
            }
        });
    };

    // Confirmation modal: allow user
    function allowUserModal(user_id, message) {
        $('#allow-user-modal').dialog({
            user_id: user_id, // Adding the user's id when the function is called
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                "Autoriser": function () {
                    $(this).dialog("close");
                    allowUser(user_id);
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).empty();
            }
        }).append(message);
    };

    // Trigger allowUserModal() when "autoriser" / "allow" button is clicked
    $(document).on('click', 'button.allow_user', function () {
        var user_id = $(this).parents('tr').attr('id').replace('userid-', '');
        var username = $(this).parents('tr').find('.username').html();

        // Write message and open modal which triggers the next elements
        var message = "<p>Voulez-vous autoriser l'utilisateur <b>" + username + "</b> à accéder à la plateforme ?</p>";
        allowUserModal(user_id, message);

        // Unbind the event
        $(this).off();
    });

    /* 
    =================================================================
    Interactions to NOT enable a new user
    =================================================================
    */
    function rejectUser(user_id) {
        
        // Send the request to the server
        $.ajax({
            url: './admin_api.php',
            method: 'POST',
            async: true,
            data: {
                mode: 'reject_user',
                id: user_id
            },
            dataType: 'json',
            beforeSend: function() {
                // Do sth
            },
            success: function(response) {
                
                // Check if any error happened
                if (response.success == true || response.success == false && response.error_code == 2) { // No error happened, we can keep processing

                    // Display the modal with a successful message
                    var message = "<p>La demande a été rejetée et l'utilisateur a été supprimé.</p>"; 
                    displayMessageModal(message);

                    // Reload the tables
                    fetchUserData();
                    
                } else { // An error happened, display 
                    
                    console.error(response.error);
                    
                    var message = 
                        "<p>Une erreur s'est produite :</p>" +
                        "<p>" + response.error + "</p>"; 
                    displayMessageModal(message);

                }

            },
            complete: function() {
                // Do sth
            },
            error: function(response) {
                console.error(response.error);
            }
        });
    };

    // Confirmation modal: allow user
    function rejectUserModal(user_id, message) {
        $('#allow-user-modal').dialog({
            user_id: user_id, // Adding the user's id when the function is called
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                "Rejeter la demande": function () {
                    $(this).dialog("close");
                    rejectUser(user_id);
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).empty();
            }
        }).append(message);
    };

    // Trigger rejectUserModal() when "refuser" / "reject" button is clicked
    $(document).on('click', 'button.reject_user', function () {
        var user_id = $(this).parents('tr').attr('id').replace('userid-', '');
        var username = $(this).parents('tr').find('.username').html();

        // Write message and open modal which triggers the next elements
        var message = "<p>Voulez-vous rejeter la demande de l'utilisateur <b>" + username + "</b> ? Son compte sera supprimé et il ne pourra pas accéder à la plateforme à moins d'effectuer une nouvelle demande.</p>";
        rejectUserModal(user_id, message);

        // Unbind the event
        $(this).off();
    });

    /* 
    =================================================================
    Interactions to revoke the user's rights to use the application
    =================================================================
    */
    function revokeUser(user_id) {
        
        // Send the request to the server
        $.ajax({
            url: './admin_api.php',
            method: 'POST',
            async: true,
            data: {
                mode: 'revoke_user',
                id: user_id
            },
            dataType: 'json',
            beforeSend: function() {
                // Do sth
            },
            success: function(response) {
               
                // Check if any error happened
                if (response.success == true || response.success == false && response.error_code == 2) { // No error happened, we can keep processing

                    // Display the modal with a successful message
                    var message = "<p>L'utilisateur ne peut maintenant plus accéder aux outils de contribution de la plateforme.</p>"; 
                    displayMessageModal(message);

                    // Reload the tables
                    fetchUserData();
                    
                } else { // An error happened, display 
                    
                    console.error(response.error);
                    
                    var message = 
                        "<p>Une erreur s'est produite :</p>" +
                        "<p>" + response.error + "</p>"; 
                    displayMessageModal(message);

                }

            },
            complete: function() {
                // Do sth
            },
            error: function(response) {
                console.error(response.error);
            }
        });
    };

    // Confirmation modal: allow user
    function revokeUserModal(user_id, message) {
        $('#allow-user-modal').dialog({
            user_id: user_id, // Adding the user's id when the function is called
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                "Retirer les droits": function () {
                    $(this).dialog("close");
                    revokeUser(user_id);
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).empty();
            }
        }).append(message);
    };

    // Trigger rejectUserModal() when "refuser" / "reject" button is clicked
    $(document).on('click', 'button.revoke_user', function () {
        var user_id = $(this).parents('tr').attr('id').replace('userid-', '');
        var username = $(this).parents('tr').find('.username').html();

        // Write message and open modal which triggers the next elements
        var message = "<p>Voulez-vous retirer les droits de l'utilisateur <b>" + username + "</b> ? Il ne pourra plus accéder aux outils de contribution de la plateforme jusqu'à ce que vous l'y autorisiez à nouveau.</p>";
        revokeUserModal(user_id, message);

        // Unbind the event
        $(this).off();
    });

    /* 
    =================================================================
    Interactions to edit an user's permissions
    =================================================================
    */
    // Confirmation modal: edit the user's role
    function editUserRoleToggle(user_id, self) {
        $('#edit-user-role-modal').dialog({
            user_id: user_id, // Adding the user's id when the function is called
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                "Editer": function () {
                    $(this).dialog("close");
                    // Make this select available
                    var list = $('tr#userid-' + user_id).find('select');
                    list.prop('disabled', false);
                    // Remove the parameters wheel
                    self.remove();
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).empty();
            }
        }).append('<p>Souhaitez-vous éditer le rôle de cet utilisateur ?</p><i>Par mesure de sécurité, les champs sont verrouillés par défaut.</i>');
    };

    // Toggle the modal when we click on the "parameters" wheel
    $(document).on('click', '.edit-user-role-toggle', function () {
        var self = $(this);
        var user_id = $(this).parents('tr').attr('id').replace('userid-','');
        editUserRoleToggle(user_id, self);

        // Unbind the event
        self.off();
    });

    // Edit an user's role on click
    $(document).on('change', '.select-user-role', function () {
        var self = $(this);
        var role = self.val();
        var user_id = self.parents('tr').attr('id').replace('userid-','');

        // Send the query to the server
        $.ajax({
            url: './admin_api.php',
            method: 'POST',
            async: true,
            data: {
                mode: 'edit_user_role',
                role: role,
                id: user_id
            },
            dataType: 'json',
            beforeSend: function() {
                // Do sth
            },
            success: function(response) {
                // Check if an error happened or not
                if (response.success == true || response.error_code == 2) {
                    // Don't do anything
                } else {
                    displayErrorModal(response.error);
                }

            },
            complete: function() {
                // Do sth
            },
            error: function(response) {
                console.error(response.error);
                displayErrorModal(response.error);
            }
        });

        // Unbind the event
        self.off();
    });
    

    
// End of document.ready() function
});