# Bienvenue sur le dépôt de code de l'application du projet RestEAUr'Lag

Ce dépôt contient les fichiers de l'application de cartographie interactive du projet RestEAUr'Lag tels qu'ils ont été produits par Vincent Delbar (2019 - @vidlb) et Marie Gradeler (2020/21 - @GradelerM).

Vous trouverez ici le [script d'installation](scripts_install/sh/geoserver_install.sh) du serveur mis en place dans le cadre du projet, ainsi que d'autre fichiers visant à faciliter le traitement des données avec python ou le sql.

Dans le dossier [docs](docs) se trouve un [un texte explicatif](docs/IDG.md) pour comprendre l'infrastructure de données mise en place. Le reste de la documentation est accessible dans le [wiki](https://gitlab.com/GradelerM/resteaur-lag/-/wikis/home) du projet *(en cours de rédaction)*.

Les fichiers HTML, CSS et JavaScript à la racine du projet concernent la page d'accueil de l'application et permettent de rediriger l'utilisateur sur la carte.

Enfin, le répertoire [map](map) contient tous les fichiers nécessaires au bon fonctionnement de la plateforme cartographique.


## Information sur les navigateurs

L'application est testée sur **Firefox** et **Google Chrome**. **Internet Explorer n'est en revanche pas supporté.**
En théorie, elle fonctionne pour :
* Firefox > 16
* Chrome > 40
* Microsoft Edge
* Safari > 8
* Opera > 27


## Fonctionnement de l'application

L'application web cartographique RestEAUr'Lag est construite de la manière suivante :

#### Back-end

- [Debian 10 (buster)](https://www.debian.org/releases/buster/)
- [PostgreSQL](https://www.postgresql.org/) 11.9
- [PostGIS](https://postgis.net/) 2.5
- [GeoServer](http://geoserver.org/) 2.16.4 - installé dans [Tomcat](https://tomcat.apache.org/) 9.0.31
- [Apache](https://httpd.apache.org/) 2.4

#### Front-end

- [OpenLayers](https://openlayers.org/) 6.4.2
- [OpenLayers-ext](https://github.com/Viglino/OpenLayers-ext)
- [jQuery](https://jquery.com/) 3.5.1
- [jQuery UI](https://jqueryui.com/) 1.12.1
- [jQuery-csv](https://github.com/typeiii/jquery-csv) 1.0.11
- [Papa Parse](https://www.papaparse.com/) 5.0
- [D3](https://d3js.org/) 6.3.1
